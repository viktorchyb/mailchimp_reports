### Setup
clone project and install dependencies
   
    > git clone git@gitlab.com:viktorchyb/mailchimp_reports.git  
    > cd mailchimp_reports
    > python3.7 -m venv env
    > source env/bin/activate  
    > pip install -r requirements.txt
    
create file `local_settings.py` inside `mailchimp_reports/mailchimp` and copy contents of `local_settings_examplepy` replacing `PLACEHOLDER` str with your credentials

after that you can start project

    > python manage.py runserver

### Open API docs in your browser

* [http://127.0.0.1:8000/](http://127.0.0.1:8000/)


### example request
    127.0.0.1:8000/reports/progress/<str:campaign_id>/
    
### example response
    {
        "sent_time": "2019-11-12T15:01:37Z",
        "campaign_id": "a266d55c87",
        "campaign_title": "test_4",
        "emails_sent": 2,
        "abuse_reports": 0,
        "unsubscribed": 0,
        "bounces": 0,
        "forwards": 0,
        "opens": 2,
        "clicks": 1,
        "delivered": 2,
        "unopened": 0,
        "members": [
            {
                "first_name": "victor",
                "last_name": "chyb",
                "email": "viktor.chyb@starnavi.io",
                "last_open": "2019-11-12T15:01:55Z",
                "member_rating": 4,
                "open_count": 2,
                "click_count": 1
            },
        ]
    }
