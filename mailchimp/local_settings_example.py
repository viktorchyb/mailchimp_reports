SECRET_KEY = 'ibfqz)tp(m04y74nwlc+obf6q$y7$ul%!ks527w4(++nnem*h6'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'mailchimp',
        'USER': 'PLACEHOLDER',
        'PASSWORD': 'PLACEHOLDER',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']

CELERY_BROKER_URL = 'redis://127.0.0.1:6379'

DEBUG = True

MAILCHIMP_API_KEY = 'PLACEHOLDER'
MAILCHIMP_API_USERNAME = 'PLACEHOLDER'

