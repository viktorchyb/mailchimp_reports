from django.urls import path, include

from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='mailchimp reports')

urlpatterns = [
    path(r'', schema_view),
    path(r'reports/', include('reports.urls')),
]

