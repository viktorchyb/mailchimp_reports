from mailchimp import celery_app

from ..extractors import ReportDataExtractor, DailyActivityExtractor
from ..integrations import get_report_data_for_campaign
from ..services import create_or_update_campaign_report, update_or_create_campaign_performance

from .utils import create_members_data


@celery_app.task
def generate_campaign_report(*, campaign_id: str):
    raw_report_data = get_report_data_for_campaign(campaign_id=campaign_id)
    list_id = raw_report_data['list_id']

    extracted_report = ReportDataExtractor(raw_data=raw_report_data).extract_data()
    report = create_or_update_campaign_report(data=extracted_report)
    get_daily_campaign_performance.delay(timeseries=raw_report_data['timeseries'], campaign_id=campaign_id)

    members = create_members_data(campaign_id=campaign_id, list_id=list_id)
    for member in members:
        if not report.members.filter(member_id=member.id).exists():
            report.members.add(member)


@celery_app.task
def get_daily_campaign_performance(*, timeseries: list, campaign_id: str):
    extracted = DailyActivityExtractor(raw_data=timeseries).extract_data()
    extracted['campaign_id'] = campaign_id

    update_or_create_campaign_performance(data=extracted)
