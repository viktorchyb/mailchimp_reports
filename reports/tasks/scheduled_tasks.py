from mailchimp import celery_app

from ..integrations import get_all_campaign_ids


@celery_app.task
def generate_campaign_reports():
    from .tasks import generate_campaign_report
    campaign_ids = get_all_campaign_ids()

    for campaign_id in campaign_ids:
        generate_campaign_report.delay(campaign_id=campaign_id)
