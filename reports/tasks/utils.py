from ..extractors import MemberExtractor
from ..integrations import get_campaign_members_data_from_list, get_campaign_sent_to, get_campaign_email_activity
from ..services import create_or_update_campaign_members
from ..utils import find_key_in_dict_list_by_email


def create_members_data(campaign_id: str, list_id: str):
    raw_sent_to = get_campaign_sent_to(campaign_id=campaign_id)
    raw_members = get_campaign_members_data_from_list(list_id=list_id)
    raw_email_activity = get_campaign_email_activity(campaign_id=campaign_id)

    result_members_list = []
    for sent_to_member in raw_sent_to:
        email = sent_to_member['email_address']
        activities = find_key_in_dict_list_by_email(data=raw_email_activity, key='activity', email=email)
        extracted = MemberExtractor(raw_data=sent_to_member, raw_activities=activities).extract_data()
        extracted['member_rating'] = find_key_in_dict_list_by_email(data=raw_members, key='member_rating', email=email)
        result_members_list.append(create_or_update_campaign_members(data=extracted))

    return result_members_list
