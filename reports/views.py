from rest_framework.response import Response
from rest_framework import mixins, viewsets

from .serializers import ProgressChartSerializer, PerformanceChartSerializer
from .services import get_recent_report_by_campaign_id, get_performance_data_for_campaign_for_days


class ProgressChartAPI(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = ProgressChartSerializer
    lookup_field = 'campaign_id'

    def retrieve(self, request, *args, **kwargs):
        report = get_recent_report_by_campaign_id(campaign_id=kwargs['campaign_id'])
        data = self.get_serializer(report)
        return Response(data.data)


class PerformanceChartAPI(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = PerformanceChartSerializer
    queryset = ''
    lookup_field = 'campaign_id'

    def retrieve(self, request, *args, **kwargs):
        campaign_performance = get_performance_data_for_campaign_for_days(campaign_id=kwargs['campaign_id'])
        data = self.get_serializer(campaign_performance, many=True)
        return Response(data.data)
