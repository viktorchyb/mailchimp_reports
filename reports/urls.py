from rest_framework.routers import DefaultRouter

from .views import ProgressChartAPI, PerformanceChartAPI


router = DefaultRouter()
router.register(r'progress', ProgressChartAPI, basename='progress')
router.register(r'performance', PerformanceChartAPI, basename='performance')

urlpatterns = router.urls
