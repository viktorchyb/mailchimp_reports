from rest_framework.serializers import ModelSerializer

from .models import CampaignReport, CampaignMember, DailyCampaignPerformance


class MemberSerializer(ModelSerializer):
    class Meta:
        model = CampaignMember
        fields = (
            'first_name', 'last_name', 'email', 'last_open',
            'member_rating', 'open_count', 'click_count',
        )


class ProgressChartSerializer(ModelSerializer):
    members = MemberSerializer(read_only=True, many=True)

    class Meta:
        model = CampaignReport
        fields = (
            'sent_time', 'campaign_id', 'campaign_title', 'emails_sent',
            'abuse_reports', 'unsubscribed', 'bounces', 'forwards',
            'opens', 'clicks', 'delivered', 'unopened', 'members',
        )


class PerformanceChartSerializer(ModelSerializer):
    class Meta:
        model = DailyCampaignPerformance
        fields = (
            'opens', 'clicks', 'date_of_report'
        )
