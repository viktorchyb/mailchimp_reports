from typing import Union
from .utils import get_action_for_value, get_date_from_timestamp


class BaseExtractor:
    def __init__(self, *, raw_data: Union[dict, list]):
        self.raw_data = raw_data

    def extract_data(self):
        raise NotImplementedError


class ReportDataExtractor(BaseExtractor):
    def extract_data(self):
        return {
            'emails_sent': self.raw_data['emails_sent'],
            'unsubscribed': self.raw_data['unsubscribed'],
            'abuse_reports': self.raw_data['abuse_reports'],
            'bounces': self.raw_data['bounces']['hard_bounces'],
            'forwards': self.raw_data['forwards']['forwards_count'],
            'opens': self.raw_data['opens']['opens_total'],
            'clicks': self.raw_data['clicks']['clicks_total'],
            'sent_time': self.raw_data['send_time'],
            'campaign_id': self.raw_data['id'],
            'campaign_title': self.raw_data['campaign_title'],
            'campaign_subject': self.raw_data['subject_line'],
        }


class DailyActivityExtractor(BaseExtractor):
    def extract_data(self):
        opens = clicks = 0
        for item in self.raw_data:
            opens += item['unique_opens']
            clicks += item['recipients_clicks']

        return {
            'opens': opens,
            'clicks': clicks,
            'date_of_report': get_date_from_timestamp(timestamp=self.raw_data[-1]['timestamp'])
        }


class MemberExtractor(BaseExtractor):
    def __init__(self, *, raw_data: dict, raw_activities: dict):
        super().__init__(raw_data=raw_data)
        self.raw_activities = raw_activities

    def extract_data(self):
        data = {
            'member_id': self.raw_data['email_id'],
            'email': self.raw_data['email_address'],
            'first_name': self.raw_data['merge_fields']['FNAME'],
            'last_name': self.raw_data['merge_fields']['LNAME'],
            'open_count': get_action_for_value(self.raw_activities, 'open'),
            'click_count': get_action_for_value(self.raw_activities, 'click')
        }
        if self.raw_data['last_open']:
            data['last_open'] = self.raw_data['last_open']
        return data

