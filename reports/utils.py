from datetime import datetime


def get_action_for_value(data: dict, value: str):
    count = 0
    for item in data:
        if item['action'] == value:
            count += 1
    return count


def find_key_in_dict_list_by_email(*, data: dict, key: str, email: str):
    return [i[key] for i in data if i['email_address'] == email][0]


def get_date_from_timestamp(*, timestamp: str):
    return datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S+00:00').date()
