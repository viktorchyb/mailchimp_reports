from datetime import date, timedelta

from .models import CampaignReport, CampaignMember, DailyCampaignPerformance


def get_recent_report_by_campaign_id(*, campaign_id: str):
    return CampaignReport.objects.filter(
        campaign_id=campaign_id
    ).order_by('-sent_time').first()


def get_performance_data_for_campaign_for_days(*, campaign_id: str, days: int = 30):
    past_month = date.today() - timedelta(days=days)
    return DailyCampaignPerformance.objects.filter(campaign_id=campaign_id, date_of_report__gte=past_month)


def create_or_update_campaign_report(*, data: dict):
    return CampaignReport.objects.update_or_create(
        campaign_id=data.pop('campaign_id'),
        defaults=data
    )[0]


def create_or_update_campaign_members(*, data: dict):
    return CampaignMember.objects.update_or_create(
        email=data['email'],
        defaults=data
    )[0]


def update_or_create_campaign_performance(*, data: dict):
    return DailyCampaignPerformance.objects.update_or_create(
        date_of_report=data.pop('date_of_report'),
        campaign_id=data.pop('campaign_id'),
        defaults=data
    )[0]
