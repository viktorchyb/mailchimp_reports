RAW_MEMBER_DATA = {
    "email_id": "email_id",
    "email_address": "test@email.com",
    "merge_fields": {
        "FNAME": "name",
        "LNAME": "surname",
        "ADDRESS": {},
        "PHONE": "",
        "BIRTHDAY": ""
    },
    "vip": False,
    "status": "sent",
    "open_count": 2,
    "last_open": "2019-11-12T15:01:55+00:00",
    "absplit_group": "",
    "gmt_offset": 0,
    "campaign_id": "123qwe",
    "list_id": "list_id",
    "list_is_active": True,
    "_links": []
}


RAW_ACTIVITIES = [
    {
        "campaign_id": "123qwe",
        "list_id": "34084c6eaf",
        "list_is_active": True,
        "email_id": "email_id",
        "email_address": "test@email.com",
        "activity": [
            {"action": "open", "timestamp": "2019-11-12T15:01:53+00:00", "ip": "66.102.9.173"},
            {"action": "open", "timestamp": "2019-11-12T15:01:55+00:00", "ip": "46.175.248.188"},
            {"action": "click", "timestamp": "2019-11-12T15:01:55+00:00", "url": "https://gitlab.com/viktorchyb/mailchimp_reports", "ip": "46.175.248.188"}
        ],
        "_links": []
    }
]


RAW_REPORT = {
    "id": "123qwe",
    "campaign_title": "TEST_TITLE",
    "type": "regular",
    "list_id": "34084c6eaf",
    "list_is_active": True,
    "list_name": "test_business_name",
    "subject_line": "TEST_SUBJECT",
    "preview_text": "",
    "emails_sent": 10,
    "abuse_reports": 8,
    "unsubscribed": 9,
    "send_time": "2019-11-12T15:01:37+00:00",
    "bounces": {
        "hard_bounces": 7, "soft_bounces": 0, "syntax_errors": 0
    },
    "forwards": {
        "forwards_count": 6, "forwards_opens": 0
    },
    "opens": {
        "opens_total": 5, "unique_opens": 1, "open_rate": 0.5, "last_open": "2019-11-12T15:01:55+00:00"
    },
    "clicks": {
        "clicks_total": 4, "unique_clicks": 1, "unique_subscriber_clicks": 1, "click_rate": 0.5, "last_click": "2019-11-12T15:01:55+00:00"
    },
    "facebook_likes": {
        "recipient_likes": 0, "unique_likes": 0, "facebook_likes": 0
    },
    "list_stats": {
        "sub_rate": 0, "unsub_rate": 0, "open_rate": 0, "click_rate": 0
    },
    "timeseries": [],
    "ecommerce": {
        "total_orders": 0, "total_spent": 0, "total_revenue": 0, "currency_code": "USD"
    },
    "delivery_status": {"enabled": False},
    "_links": []
}


RAW_TIMESERIES = [
    {
        'timestamp': '2019-11-13T07:00:00+00:00',
        'emails_sent': 3,
        'unique_opens': 1,
        'recipients_clicks': 1
    },
    {
        'timestamp': '2019-11-13T08:00:00+00:00',
        'emails_sent': 3,
        'unique_opens': 1,
        'recipients_clicks': 0
    },
    {
        'timestamp': '2019-11-13T09:00:00+00:00',
        'emails_sent': 3,
        'unique_opens': 1,
        'recipients_clicks': 1
    }
]
