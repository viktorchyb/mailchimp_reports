from datetime import date

from django.test import TestCase

from reports.extractors import ReportDataExtractor, MemberExtractor, DailyActivityExtractor

from ..utils import find_key_in_dict_list_by_email
from .fixtures import RAW_REPORT, RAW_MEMBER_DATA, RAW_ACTIVITIES, RAW_TIMESERIES


class TestReportDataExtractor(TestCase):
    def test_extract_data(self):
        actual = ReportDataExtractor(raw_data=RAW_REPORT).extract_data()
        expected = {
            'emails_sent': 10,
            'unsubscribed': 9,
            'abuse_reports': 8,
            'bounces': 7,
            'forwards': 6,
            'opens': 5,
            'clicks': 4,
            'sent_time': '2019-11-12T15:01:37+00:00',
            'campaign_id': "123qwe",
            'campaign_title': 'TEST_TITLE',
            'campaign_subject': 'TEST_SUBJECT',
        }

        self.assertDictEqual(actual, expected)


class TestMemberDataExtractor(TestCase):
    def test_extract_data(self):
        activities = find_key_in_dict_list_by_email(data=RAW_ACTIVITIES, key='activity', email='test@email.com')
        actual = MemberExtractor(raw_data=RAW_MEMBER_DATA, raw_activities=activities).extract_data()
        expected = {
            'member_id': 'email_id',
            'email': 'test@email.com',
            'first_name': 'name',
            'last_name': 'surname',
            'open_count': 2,
            'click_count': 1,
            'last_open': '2019-11-12T15:01:55+00:00'
        }

        self.assertDictEqual(actual, expected)

    def test_extract_data_no_last_open(self):
        member_data = RAW_MEMBER_DATA.copy()
        member_data['last_open'] = None

        activities = find_key_in_dict_list_by_email(data=RAW_ACTIVITIES, key='activity', email='test@email.com')
        actual = MemberExtractor(raw_data=member_data, raw_activities=activities).extract_data()
        expected = {
            'member_id': 'email_id',
            'email': 'test@email.com',
            'first_name': 'name',
            'last_name': 'surname',
            'open_count': 2,
            'click_count': 1,
        }

        self.assertDictEqual(actual, expected)


class TestDailyActivityExtractor(TestCase):
    def test_extract_data(self):
        actual = DailyActivityExtractor(raw_data=RAW_TIMESERIES).extract_data()
        expected = {
            'opens': 3,
            'clicks': 2,
            'date_of_report': date(2019, 11, 13)
        }

        self.assertDictEqual(actual, expected)
