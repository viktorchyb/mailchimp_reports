from django.db import models


class CampaignMember(models.Model):
    member_id = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=255)
    member_rating = models.IntegerField()
    open_count = models.IntegerField()
    click_count = models.IntegerField()
    last_open = models.DateTimeField(null=True, blank=True)


class DailyCampaignPerformance(models.Model):
    opens = models.IntegerField()
    clicks = models.IntegerField()
    date_of_report = models.DateField()
    campaign_id = models.CharField(max_length=100)


class CampaignReport(models.Model):
    campaign_id = models.CharField(max_length=100)
    campaign_title = models.CharField(max_length=100)
    campaign_type = models.CharField(max_length=100)
    campaign_subject = models.CharField(max_length=100)
    sent_time = models.DateTimeField()
    emails_sent = models.IntegerField()
    abuse_reports = models.IntegerField()
    unsubscribed = models.IntegerField()
    bounces = models.IntegerField()
    forwards = models.IntegerField()
    opens = models.IntegerField()
    clicks = models.IntegerField()
    members = models.ManyToManyField(CampaignMember)

    @property
    def delivered(self):
        return self.emails_sent - self.bounces

    @property
    def unopened(self):
        return self.delivered - self.opens
