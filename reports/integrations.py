import requests

from django.conf import settings

from mailchimp3 import MailChimp


def get_all_campaign_ids():
    """
    call to some service to get campaign ids
    """
    # campaigns_url = 'where/do/i/get/campaign_ids'
    # params = {}
    # response = requests.get(url=campaigns_url, params=params)
    # return response.json()

    # remove temporary code below to call service with campaign ids instead
    from .models import CampaignReport
    return [report.campaign_id for report in CampaignReport.objects.values_list('campaign_id', flat=True).distinct()]


def get_mailchimp_api_client() -> MailChimp:
    return MailChimp(
        mc_api=settings.MAILCHIMP_API_KEY,
        mc_user=settings.MAILCHIMP_API_USERNAME
    )


def get_report_data_for_campaign(*, campaign_id: str):
    client = get_mailchimp_api_client()
    report_data = client.reports.get(campaign_id=campaign_id)
    return report_data


def get_campaign_members_data_from_list(*, list_id: str):
    client = get_mailchimp_api_client()
    response = client.lists.members.all(list_id=list_id)
    return response['members']


def get_campaign_sent_to(*, campaign_id: str):
    client = get_mailchimp_api_client()
    response = client.reports.sent_to.all(campaign_id=campaign_id, get_all=True)
    return response['sent_to']


def get_campaign_email_activity(*, campaign_id: str):
    client = get_mailchimp_api_client()
    response = client.reports.email_activity.all(campaign_id=campaign_id, get_all=True)
    return response['emails']
